<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Latitude</label>
            <input class="form-control" name="latitude" id="Latitude">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Longitude</label>
            <input class="form-control" name="longitude" id="Longitude">
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label>Position</label>
            <input class="form-control" name="position" id="Position">
        </div>
    </div>

    <div class="col-sm-12">
        <br>
        <div id="map" style="width: 100%; height: 100vh;"></div>
    </div>
</div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var stamen = L.tileLayer('https://stamen-tiles-{S}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    const map = L.map('map', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 20,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Stamen': stamen
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);
    
    var rad = L.circle([-7.388889078548703, 109.96373235416648], {
        radius: 150,
        // untuk warna bisa diisi dengan kode warna
        color: 'red', // warna tepi luar
        fillColor: 'grey', // warna dalam
        fillOpacity: 0.7 // ketebalan warna dalam
        }).addTo(map);

    var marker = L.marker([-7.388889078548703, 109.96373235416648], {
        draggable: true,
    });


    marker.on('dragend', function(event) {
        var latlng = event.target.getLatLng();
        var distance = latlng.distanceTo(rad.getLatLng());
        if (distance <= rad.getRadius()) {
            document.getElementById('Latitude').value = latlng.lat;
            document.getElementById('Longitude').value = latlng.lng;
            document.getElementById('Position').value = latlng.lat + ', ' + latlng.lng;
        } else {
            alert("Don't go outside the boundary area!");
            event.target.setLatLng(rad.getLatLng());
            document.getElementById('Latitude').value = '';
            document.getElementById('Longitude').value = '';
            document.getElementById('Position').value = '';
        }
    });
    

    map.on('click', function(event) {
        var latlng = event.latlng;
        var distance = latlng.distanceTo(rad.getLatLng());
        if (distance <= rad.getRadius()) {
            if (!marker) {
                marker = L.marker(event.latlng).addTo(map);
            } else {
                marker.setLatLng(event.latlng);
            }
            document.getElementById('Latitude').value = latlng.lat;
            document.getElementById('Longitude').value = latlng.lng;
            document.getElementById('Position').value = latlng.lat + ', ' + latlng.lng;
        } else {
            alert("Don't go outside the boundary area!");
            event.target.setLatLng(rad.getLatLng());
            document.getElementById('Latitude').value = '';
            document.getElementById('Longitude').value = '';
            document.getElementById('Position').value = '';
        }
    });

    map.addLayer(marker);
</script>