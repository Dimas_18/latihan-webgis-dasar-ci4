<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    const map = L.map('map', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 17,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    L.polyline([
        [-7.3891710317127535, 109.96372430751039],
        [-7.3894636244135175, 109.96390133329034],
        [-7.3899823109977065, 109.96431171123473],
        [-7.390732369279719, 109.96326387003774],
        [-7.390582508913745, 109.96299547032702],
        [-7.390432648496935, 109.96295036113196],
        [-7.390251473895293, 109.96278345711016],
        [-7.390021091269839, 109.96268196142123],
        [-7.389893597920612, 109.96272932607607],
        [-7.389605060204925, 109.96305862320014]
    ], {
        color: 'green',
        weight: 5 // tebal garis
        })
        .bindPopup("<img src='<?= base_url('img/jalanpasarkertek.jpg'); ?>' width='100%'>" + "<h5>Jalan Sekitar Pasar Kertek</h5>" + "KM 3<br>" + "Kab. Wonosobo")
        .addTo(map);
</script>