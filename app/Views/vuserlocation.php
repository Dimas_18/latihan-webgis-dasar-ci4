<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>User Position</label>
            <input class="form-control" name="position" id="Position">
        </div>
    </div>
</div>

<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var stamen = L.tileLayer('https://stamen-tiles-{S}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });
    
    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Stamen': stamen
    };

    
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    
    function showPosition(position) {
        document.getElementById("Position").value = position.coords.latitude + ", " + position.coords.longitude;
        const map = L.map('map', {
            center: [position.coords.latitude, position.coords.longitude],
            zoom: 10,
            layers: [defaultmap]
        });
        
        const layerControl = L.control.layers(baseLayers, null, {
            collapsed: false
        }).addTo(map);
        
        L.marker([position.coords.latitude, position.coords.longitude])
        .addTo(map)
        .bindPopup("<img src='<?= base_url('img/user.png'); ?>' width='100%'>" + "<h5>You here now!</h5>")
        .openPopup();
    }
</script>