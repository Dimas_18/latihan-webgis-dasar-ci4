<div class="row">
    <div class="col-sm-8">
        <div id="map" style="width: 100%; height: 100vh;"></div>
    </div>

    <div class="col-sm-4">
        <div class="row">
            <?php
            if (session()->getFlashdata('message')) {
                echo '<div class="alert alert-success">';
                echo session()->getFlashdata('message');
                echo '</div>';
            }
            ?>
            <?php $errors = validation_errors() ?>
            <?php echo form_open_multipart('clocation/updatedata/'.$location['id']) ?>

            <div class="form-group">
                <label>Location Name</label>
                <input class="form-control" name="locationname" value="<?= $location['locationname'] ?>">
                <p class="text-danger"><?= isset($errors['locationname']) == isset($errors['locationname']) ? validation_show_error('locationname') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Address Location</label>
                <input class="form-control" name="addresslocation" value="<?= $location['addresslocation'] ?>">
                <p class="text-danger"><?= isset($errors['addresslocation']) == isset($errors['addresslocation']) ? validation_show_error('addresslocation') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Latitude</label>
                <input class="form-control" name="latitude" id="Latitude" value="<?= $location['latitude'] ?>">
                <p class="text-danger"><?= isset($errors['latitude']) == isset($errors['latitude']) ? validation_show_error('latitude') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Longitude</label>
                <input class="form-control" name="longitude" id="Longitude" value="<?= $location['longitude'] ?>">
                <p class="text-danger"><?= isset($errors['longitude']) == isset($errors['longitude']) ? validation_show_error('longitude') : '' ?></p>
            </div>

            <div class="form-group">
                <label>Photo</label>
                <input type="file" class="form-control" name="photo" accept="image/*">
                <p class="text-danger"><?= isset($errors['photo']) == isset($errors['photo']) ? validation_show_error('photo') : '' ?></p>
                <img src="<?= base_url('photo/'.$location['photo']); ?>" width="200px">
            </div>

            <br>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<?= base_url('clocation/index'); ?>" class="btn btn-success">Back</a>

            <?php echo form_close() ?>
        </div>
    </div>
</div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var stamen = L.tileLayer('https://stamen-tiles-{S}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    var map = L.map('map', {
        center: [<?= $location['latitude'] ?>, <?= $location['longitude'] ?>],
        zoom: 10,
        layers: [defaultmap]
    });

    var baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Stamen': stamen
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    var latInput = document.querySelector("[name=latitude]");
    var lngInput = document.querySelector("[name=longitude]");
    var posInput = document.querySelector("[name=position]");
    var loc = [<?= $location['latitude'] ?>, <?= $location['longitude'] ?>];
    map.attributionControl.setPrefix(false);
    
    var marker = new L.marker(loc, {
        draggable: true,
    });

    marker.on('dragend', function(e) {
        var position = marker.getLatLng();
        marker.setLatLng(position, {
            loc,
        }).bindPopup(position).update();
        $("#Latitude").val(position.lat);
        $("#Longitude").val(position.lng);
    });

    map.on('click', function(e) {
        var lat = e.latlng.lat;
        var lng = e.latlng.lng;
        if (!marker) {
            marker = L.marker(e.latlng).addTo(map);
        } else {
            marker.setLatLng(e.latlng);
        }
        latInput.value = lat;
        lngInput.value = lng;
        posInput.value = lat + ', ' + lng;
    });

    map.addLayer(marker);
</script>