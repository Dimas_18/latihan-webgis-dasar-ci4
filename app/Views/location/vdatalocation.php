<div class="row">
    <div class="col-12">
        <?php
        if (session()->getFlashdata('message')) {
            echo '<div class="alert alert-success">';
            echo session()->getFlashdata('message');
            echo '</div>';
        }
        ?>
        <table class="table table-bordered" id="datatablesSimple">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Location Name</th>
                    <th>Address Location</th>
                    <th>Coordinates</th>
                    <th>Photo/Image</th>
                    <th>###</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;
                foreach ($location as $key => $value) { ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $value['locationname'] ?></td>
                        <td><?= $value['addresslocation'] ?></td>
                        <td><?= $value['latitude'] ?>,<?= $value['longitude'] ?></td>
                        <td><img src="<?= base_url('photo/' . $value['photo']); ?>" width="200px"></td>
                        <td>
                            <a href="<?= base_url('clocation/editlocation/' . $value['id']); ?>" class="btn btn-warning">Edit</a>
                            <a href="<?= base_url('clocation/deletelocation/' . $value['id']); ?>" class="btn btn-danger" onclick="return confirm('Are your sure to delete data?')">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>