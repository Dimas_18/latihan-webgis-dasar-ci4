<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var stamen = L.tileLayer('https://stamen-tiles-{S}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    const map = L.map('map', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 10,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Stamen': stamen
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    const home = L.icon({
        iconUrl: '<?= base_url('img/home.png'); ?>',
        iconSize: [50, 60]
    });

    <?php foreach ($location as $key => $value) { ?>
        L.marker([<?= $value['latitude'] ?>, <?= $value['longitude'] ?>], {icon: home})
        .bindPopup("<img src='<?= base_url('photo/'.$value['photo']); ?>' width='100%'>" + "<h4><?= $value['locationname'] ?></h4>" + "<br>Alamat : <?= $value['addresslocation'] ?>")
        .addTo(map);
    <?php } ?>
</script>