<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    const map = L.map('map', {
        center: [-7.390939884593395, 109.96292098595214],
        zoom: 17,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    const place = L.icon({
        iconUrl: '<?= base_url('img/marker.gif'); ?>',
        iconSize: [50, 60]
    });

    const author = L.icon({
        iconUrl: '<?= base_url('img/user.gif'); ?>',
        iconSize: [50, 60]
    });

    L.marker([-7.389144432352236, 109.96371089635517], {
            icon: place
        })
        .bindPopup("<img src='<?= base_url('img/tugukertek.jpg'); ?>' width='100%'>" + "<h5>Tugu Kertek(pusat)</h5><br>" + "Jl. Parakan no.118<br>" + "Kab. Wonosobo")
        .addTo(map);

    L.marker([-7.391983903501723, 109.96197282506138], {
            icon: author
        })
        .bindPopup("<img src='<?= base_url('img/dimas.jpeg'); ?>' width='100%'>" + "<h5>Dimas Rizki Novyadi</h5><br>" + "Creator")
        .addTo(map);
</script>