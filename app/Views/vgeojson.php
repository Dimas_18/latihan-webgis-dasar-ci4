<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/satellite-v9'
    });

    const map = L.map('map', {
        center: [0.8039659979935496, 121.06408424855046],
        zoom: 5,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Satellite': satellite
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    $.getJSON("<?= base_url('indonesiaprovince/sumbar.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'red',
                    fillOpacity: 0.1
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/sumatra.jpeg'); ?>' width='100%'>" + "<h5>Sumatra</h5>")
        .addTo(map);
    });

    $.getJSON("<?= base_url('indonesiaprovince/jabar.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'yellow',
                    fillOpacity: 0.2
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/jawa.jpeg'); ?>' width='100%'>" + "<h5>Jawa</h5>")
        .addTo(map);
    });

    $.getJSON("<?= base_url('indonesiaprovince/kaltim.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'green',
                    fillOpacity: 0.3
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/kalimantan.jpeg'); ?>' width='100%'>" + "<h5>Kalimantan</h5>")
        .addTo(map);
    });

    $.getJSON("<?= base_url('indonesiaprovince/sulsel.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'blue',
                    fillOpacity: 0.4
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/sulawesi.jpeg'); ?>' width='100%'>" + "<h5>Sulawesi</h5>")
        .addTo(map);
    });

    $.getJSON("<?= base_url('indonesiaprovince/maluku.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'purple',
                    fillOpacity: 0.5
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/maluku.jpg'); ?>' width='100%'>" + "<h5>Maluku</h5>")
        .addTo(map);
    });

    $.getJSON("<?= base_url('indonesiaprovince/papua.geojson'); ?>", function(data) {
        L.geoJSON(data, {
            style: function(feature) {
                return {
                    color: 'black',
                    fillOpacity: 0.6
                }
            }
        })
        .bindPopup("<img src='<?= base_url('img/papua.jpg'); ?>' width='100%'>" + "<h5>Papua</h5>")
        .addTo(map);
    });
</script>