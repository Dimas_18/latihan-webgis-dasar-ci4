<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Distance (in meter)</label>
            <input class="form-control" name="distance" id="Distance">
        </div>
    </div>
</div>

<div id="map" style="width: 100%; height: 100vh;"></div>

<script>
    var defaultmap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    var stamen = L.tileLayer('https://stamen-tiles-{S}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png', { // lihat disini https://github.com/CartoDB/basemap-styles
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd'
    });

    const map = L.map('map', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 9,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Stamen': stamen
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: false
    }).addTo(map);

    var routecontrol = L.Routing.control({
        waypoints: [
            L.latLng(-7.389141772464927, 109.96370821423844), // lokasi asal
            L.latLng(-7.804908793883475, 110.3788353) // lokasi tujuan
        ]
    }).addTo(map);

    routecontrol.on('routesfound', function(e) {
        var routes = e.routes;
        var summary = routes[0].summary;
        var total = summary.totalDistance;
        document.getElementById('Distance').value = total; // kirim dan tampilkan data
        animation(routes[0]);
    });

    function animation(route) {
        var iconvehicle = L.icon({
            iconUrl: '<?= base_url('img/car.png'); ?>',
            iconSize: [30, 30]
        });
        var car = L.marker([route.coordinates[0].lat, route.coordinates[0].lng], {
            icon: iconvehicle
        }).addTo(map);

        var index = 0;
        var maxindex = route.coordinates.length - 1;

        function animated() {
            car.setLatLng([route.coordinates[index].lat, route.coordinates[index].lng]);
            index++;
            if (index > maxindex) {
                index = 0;
            }
            setTimeout(animated, 10); // waktu pergerakan ikon dari posisi asal sampai tujuan
        }
        animated();
    }
</script>