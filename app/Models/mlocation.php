<?php

namespace App\Models;

use CodeIgniter\Model;

class Mlocation extends Model
{
    public function insertdata($data) {
        $this->db->table('coba')->insert($data);
    }

    public function getalldata() {
        return $this->db->table('coba')->get()->getResultArray();
    }

    public function getdatabyid($id) {
        return $this->db->table('coba')->where('id', $id)->get()->getRowArray();
    }

    public function updatedata($data) {
        $this->db->table('coba')->where('id', $data['id'])->update($data);
    }

    public function deletedata($data) {
        $this->db->table('coba')->where('id', $data['id'])->delete($data);
    }
    
    // protected $DBGroup          = 'default';
    // protected $table            = 'mlocations';
    // protected $primaryKey       = 'id';
    // protected $useAutoIncrement = true;
    // protected $returnType       = 'array';
    // protected $useSoftDeletes   = false;
    // protected $protectFields    = true;
    // protected $allowedFields    = [];

    // // Dates
    // protected $useTimestamps = false;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
}
