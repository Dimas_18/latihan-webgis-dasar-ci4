<?php

namespace App\Controllers;

use App\Models\Mlocation;

class Clocation extends BaseController
{
    public function __construct()
    {
        $this->Mlocation = new Mlocation();
    }

    public function index()
    {

        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Data Location',
            'page' => 'location/vdatalocation',
            'location' => $this->Mlocation->getalldata()
        ];

        return view('vtemplate', $data);
    }

    public function inputlocation()
    {

        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Input Location',
            'page' => 'location/vinputlocation'
        ];

        return view('vtemplate', $data);
    }

    public function insertdata()
    {

        if ($this->validate([
            'locationname' => [
                'label' => 'Location Name',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'addresslocation' => [
                'label' => 'Address Location',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'latitude' => [
                'label' => 'Latitude',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'longitude' => [
                'label' => 'Longitude',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'photo' => [
                'label' => 'Photo',
                'rules' => 'uploaded[photo]|max_size[photo,5000]|mime_in[photo,image/jpg,image/jpeg,image/png]', // satuan upload file adalah kilobyte
                'errors' => [
                    'uploaded' => '{field} file cannot empty!',
                    'max_size' => 'Max size {field} file at least 5MB!',
                    'mime_in' => 'Format {field} file is JPG, JPEG and PNG!'
                ]
            ]
        ])) {
            $photo = $this->request->getFile('photo');
            $photofilename = $photo->getRandomName();
            $data = [
                'locationname' => $this->request->getPost('locationname'),
                'addresslocation' => $this->request->getPost('addresslocation'),
                'latitude' => $this->request->getPost('latitude'),
                'longitude' => $this->request->getPost('longitude'),
                'photo' => $photofilename
            ];
            $photo->move('photo', $photofilename); // maksud 'photo' ini artinya membuat folder photo dan memindahkan file foto kedalam folder tersebut
            $this->Mlocation->insertdata($data);
            session()->setFlashdata('message', 'Adding data is successfully!');
            return redirect()->to('clocation/inputlocation');
        } else {
            return redirect()->to('clocation/inputlocation')->withInput();
        }
    }

    public function mapping() {
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Mapping',
            'page' => 'location/vmapping', // lihat file view dgn nama vmapping di folder location
            'location' => $this->Mlocation->getalldata()
        ];
        return view('vtemplate', $data);
    }

    public function editlocation($id)
    {

        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Edit Location',
            'page' => 'location/veditlocation',
            'location' => $this->Mlocation->getdatabyid($id)
        ];

        return view('vtemplate', $data);
    }

    public function updatedata($id)
    {

        if ($this->validate([
            'locationname' => [
                'label' => 'Location Name',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'addresslocation' => [
                'label' => 'Address Location',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'latitude' => [
                'label' => 'Latitude',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'longitude' => [
                'label' => 'Longitude',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} cannot empty!'
                ]
            ],
            'photo' => [
                'label' => 'Photo',
                'rules' => 'max_size[photo,5000]|mime_in[photo,image/jpg,image/jpeg,image/png]', // satuan upload file adalah kilobyte
                'errors' => [
                    'max_size' => 'Max size {field} file at least 5MB!',
                    'mime_in' => 'Format {field} file is JPG, JPEG and PNG!'
                ]
            ]
        ])) {
            $photo = $this->request->getFile('photo');
            $location = $this->Mlocation->getdatabyid($id);
            if ($photo->getError() == 4) {
                $photofilename = $location['photo'];
            } else {
                $photofilename = $photo->getRandomName();
                $photo->move('photo', $photofilename); // maksud 'photo' ini artinya membuat folder photo dan memindahkan file foto kedalam folder tersebut
            }
            $data = [
                'id' => $id,
                'locationname' => $this->request->getPost('locationname'),
                'addresslocation' => $this->request->getPost('addresslocation'),
                'latitude' => $this->request->getPost('latitude'),
                'longitude' => $this->request->getPost('longitude'),
                'photo' => $photofilename
            ];
            $this->Mlocation->updatedata($data);
            session()->setFlashdata('message', 'Update data is successfully!');
            return redirect()->to('clocation/index');
        } else {
            return redirect()->to('clocation/editlocation/'.$id)->withInput();
        }
    }

    public function deletelocation($id) {
        $data = ['id' => $id];
        $this->Mlocation->deletedata($data);
        session()->setFlashdata('message', 'Data has been deleted!');
        return redirect()->to('clocation/index');
    }
}
