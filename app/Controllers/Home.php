<?php

namespace App\Controllers;

class Home extends BaseController
{
    
    public function index()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Dashboard',
            'page' => 'vdashboard'
        ];
        
        return view('vtemplate', $data);

        // return view('welcome_message');
    }

    public function viewmap()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'View Map',
            'page' => 'vviewmap'
        ];
        
        return view('vtemplate', $data);

    }

    public function basemap()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Base Map',
            'page' => 'vbasemap'
        ];
        
        return view('vtemplate', $data);

    }

    public function marker()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Marker',
            'page' => 'vmarker'
        ];
        
        return view('vtemplate', $data);

    }

    public function circle()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Circle',
            'page' => 'vcircle'
        ];
        
        return view('vtemplate', $data);

    }

    public function polyline()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Polyline',
            'page' => 'vpolyline'
        ];
        
        return view('vtemplate', $data);

    }

    public function polygon()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Polygon',
            'page' => 'vpolygon'
        ];
        
        return view('vtemplate', $data);

    }

    public function geojson()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'GeoJSON',
            'page' => 'vgeojson'
        ];
        
        return view('vtemplate', $data);

    }

    public function coordinat()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Coordinat',
            'page' => 'vcoordinat'
        ];
        
        return view('vtemplate', $data);

    }

    public function radius()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Radius',
            'page' => 'vradius'
        ];
        
        return view('vtemplate', $data);

    }

    public function userlocation()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'User Location',
            'page' => 'vuserlocation'
        ];
        
        return view('vtemplate', $data);

    }

    public function route()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Route',
            'page' => 'vroute'
        ];
        
        return view('vtemplate', $data);

    }

    public function userroute()
    {
        
        $data = [
            'admintab' => 'Admin | Kelurahan Kertek',
            'title' => 'Route from User Location',
            'page' => 'vuserroute'
        ];
        
        return view('vtemplate', $data);

    }
}
